libalgorithm-permute-perl (0.17-1) unstable; urgency=medium

  * Team upload.
  * Import upstream version 0.17.
  * Update debian/upstream/metadata.
  * Declare compliance with Debian Policy 4.6.2.
  * Set Rules-Requires-Root: no.
  * Annotate test-only build dependencies with <!nocheck>.

 -- gregor herrmann <gregoa@debian.org>  Mon, 06 Feb 2023 18:07:58 +0100

libalgorithm-permute-perl (0.16-2) unstable; urgency=medium

  [ Damyan Ivanov ]
  * declare compliance with Policy 4.1.3 (no changes needed)

  [ Salvatore Bonaccorso ]
  * Update Vcs-* headers for switch to salsa.debian.org

  [ Laurent Baillet ]
  * fix lintian file-contains-trailing-whitespace warning

  [ gregor herrmann ]
  * debian/control: update Build-Depends for cross builds.
  * debian/watch: use uscan version 4.
  * Update 'DEB_BUILD_MAINT_OPTIONS = hardening=+bindnow' to '=+all'.

 -- Jelmer Vernooĳ <jelmer@debian.org>  Thu, 30 Jun 2022 23:22:47 +0100

libalgorithm-permute-perl (0.16-1) unstable; urgency=medium

  * Team upload

  * Import upstream version 0.16.
  * Declare conformance with Policy 4.1.1 (no changes needed)

 -- Damyan Ivanov <dmn@debian.org>  Sun, 22 Oct 2017 19:27:03 +0000

libalgorithm-permute-perl (0.15-1) unstable; urgency=medium

  * Team upload.
  * Import upstream version 0.15.

 -- gregor herrmann <gregoa@debian.org>  Wed, 02 Aug 2017 23:11:06 -0400

libalgorithm-permute-perl (0.14-1) unstable; urgency=medium

  * Team upload.

  [ Salvatore Bonaccorso ]
  * debian/control: Use HTTPS transport protocol for Vcs-Git URI

  [ gregor herrmann ]
  * debian/copyright: change Copyright-Format 1.0 URL to HTTPS.
  * Remove Dario Minnucci from Uploaders. Thanks for your work!

  * New upstream release.
    Fixes "FTBFS with Perl 5.24: undefined symbol: PUSHBLOCK"
    (Closes: #825012)
  * Update Upstream-Contact in debian/copyright.
  * debian/copyright: add stanza for new ppport.h.
  * Add debian/upstream/metadata.
  * Add new build dependency: libtest-leaktrace-perl.
  * Set bindnow linker flag in debian/rules.
  * Declare compliance with Debian Policy 4.0.0.

 -- gregor herrmann <gregoa@debian.org>  Sun, 09 Jul 2017 14:40:20 +0200

libalgorithm-permute-perl (0.12-3) unstable; urgency=medium

  * Team upload

  [ Salvatore Bonaccorso ]
  * Update Vcs-Browser URL to cgit web frontend

  [ Damyan Ivanov ]
  * mark package as autopkg-testable
  * Declare conformance with Policy 3.9.6

 -- Damyan Ivanov <dmn@debian.org>  Sat, 21 Nov 2015 19:49:47 +0000

libalgorithm-permute-perl (0.12-2) unstable; urgency=low

  [ Dario Minnucci ]
  * debian/control: 'Uploaders' updated to @d.o email address
  * debian/copyright: Updated with to @d.o email address

  [ Ansgar Burchardt ]
  * debian/control: Convert Vcs-* fields to Git.

  [ Salvatore Bonaccorso ]
  * Change Vcs-Git to canonical URI (git://anonscm.debian.org)
  * Change search.cpan.org based URIs to metacpan.org based URIs

  [ Axel Beckert ]
  * debian/copyright: migrate pre-1.0 format to 1.0 using "cme fix dpkg-
    copyright"

  [ gregor herrmann ]
  * Strip trailing slash from metacpan URLs.

  [ Florian Schlichting ]
  * Switch dh compatibility to level 9 to enable passing of hardening flags
  * Switch to source format 3.0 (quilt)
  * Ship provided benchmark.pl as example
  * Update stand-alone license paragraphs to commonly used version (not
    limiting Debian to GNU/Linux, directly linking to GPL-1)
  * Declare compliance with Debian Policy 3.9.5
  * Add myself to uploaders and copyright

 -- Florian Schlichting <fsfs@debian.org>  Fri, 07 Mar 2014 00:16:43 +0100

libalgorithm-permute-perl (0.12-1) unstable; urgency=low

  * Initial Release. (Closes: #556219)

 -- Dario Minnucci (midget) <debian@midworld.net>  Sat, 14 Nov 2009 19:33:31 +0100
